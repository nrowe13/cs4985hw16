﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProductMaintenance.aspx.cs" Inherits="ProductMaintenance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Chapter 15: Northwind</title>
    <link href="Main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <header>
        &nbsp;<img alt="Northwind Solutions" src="Images/Northwind.jpg" /><br />
    </header>
    <section>
        <form id="form1" runat="server">
            <asp:ListView ID="lvProducts" runat="server" DataKeyNames="ProductID" DataSourceID="sdsNorthwind" OnItemDeleted="ListView1_ItemDeleted" OnItemUpdated="ListView1_ItemUpdated">
                <AlternatingItemTemplate>
                    <span style="">ProductID:
                    <asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>' />
                        <br />
                        ProductName:
                    <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' />
                        <br />
                        SupplierID:
                    <asp:Label ID="SupplierIDLabel" runat="server" Text='<%# Eval("SupplierID") %>' />
                        <br />
                        CategoryID:
                    <asp:Label ID="CategoryIDLabel" runat="server" Text='<%# Eval("CategoryID") %>' />
                        <br />
                        QuantityPerUnit:
                    <asp:Label ID="QuantityPerUnitLabel" runat="server" Text='<%# Eval("QuantityPerUnit") %>' />
                        <br />
                        UnitPrice:
                    <asp:Label ID="UnitPriceLabel" runat="server" Text='<%# Eval("UnitPrice") %>' />
                        <br />
                        UnitsInStock:
                    <asp:Label ID="UnitsInStockLabel" runat="server" Text='<%# Eval("UnitsInStock") %>' />
                        <br />
                        UnitsOnOrder:
                    <asp:Label ID="UnitsOnOrderLabel" runat="server" Text='<%# Eval("UnitsOnOrder") %>' />
                        <br />
                        ReorderLevel:
                    <asp:Label ID="ReorderLevelLabel" runat="server" Text='<%# Eval("ReorderLevel") %>' />
                        <br />
                        <asp:CheckBox ID="DiscontinuedCheckBox" runat="server" Checked='<%# Eval("Discontinued") %>' Enabled="false" Text="Discontinued" />
                        <br />
                        <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                        <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                        <br />
                        <br />
                    </span>
                </AlternatingItemTemplate>
                <EditItemTemplate>
                    <span style="">
                    <table>
                    <tr><td style="text-align: left">ProductID:</td>
                    <td style="text-align: left"><asp:TextBox ID="ProductIDTextBox" runat="server" Text='<%# Bind("ProductID") %>' /><asp:RequiredFieldValidator ID="rfvID" runat="server" ErrorMessage="Please enter a valid product id." ControlToValidate="ProductIDTextBox" CssClass="error" Display="Dynamic" Text="*"/></td></tr>
                            <br />
                            <tr><td style="text-align: left">ProductName:
                   <td style="text-align: left"><asp:TextBox ID="ProductNameTextBox" runat="server" Text='<%# Bind("ProductName") %>' /><asp:RequiredFieldValidator ID="rfvName" runat="server" ErrorMessage="Please enter a valid product name." CssClass="error" ControlToValidate="ProductNameTextBox" Display="Dynamic" Text="*" /></td></tr>
                            <br />
                            <tr><td style="text-align: left">SupplierID:
                    <td style="text-align: left"><asp:TextBox ID="SupplierIDTextBox" runat="server" Text='<%# Bind("SupplierID") %>' /><asp:RequiredFieldValidator ID="rfvSupplier" runat="server" ErrorMessage="Please enter a valid supplier ID." Display="Dynamic" CssClass="error" ControlToValidate="SupplierIDTextBox" Text="*"/></td></tr>
                            <br />
                            <tr><td style="text-align: left">CategoryID:
                    <td style="text-align: left"><asp:TextBox ID="CategoryIDTextBox" runat="server" Text='<%# Bind("CategoryID") %>' /><asp:RequiredFieldValidator ID="rfvCategory" runat="server" ErrorMessage="Please enter a valid category ID." Display="Dynamic" CssClass="error" ControlToValidate="CategoryIDTextBox" Text="*"/></td></tr>
                            <br />
                            <tr><td style="text-align: left">QuantityPerUnit:
                    <td style="text-align: left"><asp:TextBox ID="QuantityPerUnitTextBox" runat="server" Text='<%# Bind("QuantityPerUnit") %>' /><asp:RequiredFieldValidator ID="rfvQuantity" runat="server" ErrorMessage="Please enter a valid quantity." ControlToValidate="QuantityPerUnitTextBox" CssClass="error" Display="Dynamic" Text="*" /></td></tr>
                            <br />
                            <tr><td style="text-align: left">UnitPrice:
                    <td style="text-align: left"><asp:TextBox ID="UnitPriceTextBox" runat="server" Text='<%# Bind("UnitPrice", "{0:c}") %>' /><asp:RequiredFieldValidator ID="rfvPrice" runat="server" ErrorMessage="Please enter a valid price." Display="Dynamic" CssClass="error" ControlToValidate="UnitPriceTextBox" Text="*"/><asp:CompareValidator ID="cvPrice" runat="server" ErrorMessage="Please enter a valid amount." CssClass="error" ControlToValidate="UnitPriceTextBox" Display="Dynamic" Operator="GreaterThanEqual" Text="*"></asp:CompareValidator></td></tr>
                            <br />
                            <tr><td style="text-align: left">UnitsInStock:
                    <td style="text-align: left"><asp:TextBox ID="UnitsInStockTextBox" runat="server" Text='<%# Bind("UnitsInStock") %>' /><asp:RequiredFieldValidator ID="rfvInStock" runat="server" ErrorMessage="Please enter a valid amount of units in stock." Display="Dynamic" CssClass="error" ControlToValidate="UnitsInStockTextBox" Text="*"/><asp:CompareValidator ID="cvInStock" runat="server" ErrorMessage="Please enter a value greater than or equal to 0" Display="Dynamic" Operator="GreaterThanEqual" ControlToValidate="UnitsInStockTextBox" ValueToCompare="0" Text="*"/></td></tr>
                            <br />
                            <tr><td style="text-align: left">UnitsOnOrder:
                    <td style="text-align: left"><asp:TextBox ID="UnitsOnOrderTextBox" runat="server" Text='<%# Bind("UnitsOnOrder") %>' /><asp:RequiredFieldValidator ID="rfvOnOrder" runat="server" ErrorMessage="Please enter a valid number of units on order." Display="Dynamic" CssClass="error" ControlToValidate="UnitsOnOrderTextBox" Text="*"/><asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please enter a value greater than or equal to 0" Display="Dynamic" Operator="GreaterThanEqual" ControlToValidate="UnitsOnOrderTextBox" ValueToCompare="0" Text="*"/></td></tr>
                            <br />
                            <tr><td style="text-align: left">ReorderLevel:
                    <td style="text-align: left"><asp:TextBox ID="ReorderLevelTextBox" runat="server" Text='<%# Bind("ReorderLevel") %>' /><asp:RequiredFieldValidator ID="rfvReorder" runat="server" ErrorMessage="Please enter a valid reorder level." Display="Dynamic" CssClass="error" ControlToValidate="ReorderLevelTextBox" Text="*"/><asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Please enter a value greater than or equal to 0" Display="Dynamic" Operator="GreaterThanEqual" ControlToValidate="ReorderLevelTextBox" ValueToCompare="0" Text="*"/></td></tr>
                            <br />
                            <tr><td><asp:CheckBox ID="CheckBoxDiscontinued" runat="server" Checked='<%# Bind("Discontinued") %>' Text="Discontinued" /></td></tr>
                    <br />
                            <tr><td>
                                <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" /></td></tr>
                            <br />
                            <br />
                        </table>
                        </span>
                </EditItemTemplate>
                <EmptyDataTemplate>
                    <span>No data was returned.</span>
                </EmptyDataTemplate>
                <InsertItemTemplate>
                    <span style="">ProductName:
                    <asp:TextBox ID="ProductNameTextBox" runat="server" Text='<%# Bind("ProductName") %>' />
                        <br />
                        SupplierID:
                    <asp:TextBox ID="SupplierIDTextBox" runat="server" Text='<%# Bind("SupplierID") %>' />
                        <br />
                        CategoryID:
                    <asp:TextBox ID="CategoryIDTextBox" runat="server" Text='<%# Bind("CategoryID") %>' />
                        <br />
                        QuantityPerUnit:
                    <asp:TextBox ID="QuantityPerUnitTextBox" runat="server" Text='<%# Bind("QuantityPerUnit") %>' />
                        <br />
                        UnitPrice:
                    <asp:TextBox ID="UnitPriceTextBox" runat="server" Text='<%# Bind("UnitPrice") %>' />
                        <br />
                        UnitsInStock:
                    <asp:TextBox ID="UnitsInStockTextBox" runat="server" Text='<%# Bind("UnitsInStock") %>' />
                        <br />
                        UnitsOnOrder:
                    <asp:TextBox ID="UnitsOnOrderTextBox" runat="server" Text='<%# Bind("UnitsOnOrder") %>' />
                        <br />
                        ReorderLevel:
                    <asp:TextBox ID="ReorderLevelTextBox" runat="server" Text='<%# Bind("ReorderLevel") %>' />
                        <br />
                        <asp:CheckBox ID="DiscontinuedCheckBox" runat="server" Checked='<%# Bind("Discontinued") %>' Text="Discontinued" />
                        <br />
                        <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" />
                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                        <br />
                        <br />
                    </span>
                </InsertItemTemplate>
                <ItemTemplate>
                    <span>
                    <table>
                    <tr><td style="text-align: left">ProductID:</td>
                    <td style="text-align: left"><asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>' /></td></tr>
                            <br />
                            <tr><td style="text-align: left">ProductName:
                   <td style="text-align: left"><asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' /></td></tr>
                            <br />
                            <tr><td style="text-align: left">SupplierID:
                    <td style="text-align: left"><asp:Label ID="SupplierIDLabel" runat="server" Text='<%# Eval("SupplierID") %>' /></td></tr>
                            <br />
                            <tr><td style="text-align: left">CategoryID:
                    <td style="text-align: left"><asp:Label ID="CategoryIDLabel" runat="server" Text='<%# Eval("CategoryID") %>' /></td></tr>
                            <br />
                            <tr><td style="text-align: left">QuantityPerUnit:
                    <td style="text-align: left"><asp:Label ID="QuantityPerUnitLabel" runat="server" Text='<%# Eval("QuantityPerUnit") %>' /></td></tr>
                            <br />
                            <tr><td style="text-align: left">UnitPrice:
                    <td style="text-align: left"><asp:Label ID="UnitPriceLabel" runat="server" Text='<%# Eval("UnitPrice", "{0:c}") %>' /></td></tr>
                            <br />
                            <tr><td style="text-align: left">UnitsInStock:
                    <td style="text-align: left"><asp:Label ID="UnitsInStockLabel" runat="server" Text='<%# Eval("UnitsInStock") %>' /></td></tr>
                            <br />
                            <tr><td style="text-align: left">UnitsOnOrder:
                    <td style="text-align: left"><asp:Label ID="UnitsOnOrderLabel" runat="server" Text='<%# Eval("UnitsOnOrder") %>' /></td></tr>
                            <br />
                            <tr><td style="text-align: left">ReorderLevel:
                    <td style="text-align: left"><asp:Label ID="ReorderLevelLabel" runat="server" Text='<%# Eval("ReorderLevel") %>' /></td></tr>
                            <br />
                            <tr><td><asp:CheckBox ID="DiscontinuedCheckBox" runat="server" Checked='<%# Eval("Discontinued") %>' Enabled="false" Text="Discontinued" /></td></tr>
                    <br />
                            <tr><td>
                                <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                                <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" /></td></tr>
                            <br />
                            <br />
                        </table>
                        </span>

                </ItemTemplate>
                <LayoutTemplate>
                    <div id="itemPlaceholderContainer" runat="server" style="">
                        <span runat="server" id="itemPlaceholder" />
                    </div>
                    <div style="">
                        <asp:DataPager ID="DataPager1" runat="server" PageSize="1">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" />
                            </Fields>
                        </asp:DataPager>
                    </div>
                </LayoutTemplate>
                <SelectedItemTemplate>
                    <span style="">ProductID:
                    <asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>' />
                        <br />
                        ProductName:
                    <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' />
                        <br />
                        SupplierID:
                    <asp:Label ID="SupplierIDLabel" runat="server" Text='<%# Eval("SupplierID") %>' />
                        <br />
                        CategoryID:
                    <asp:Label ID="CategoryIDLabel" runat="server" Text='<%# Eval("CategoryID") %>' />
                        <br />
                        QuantityPerUnit:
                    <asp:Label ID="QuantityPerUnitLabel" runat="server" Text='<%# Eval("QuantityPerUnit") %>' />
                        <br />
                        UnitPrice:
                    <asp:Label ID="UnitPriceLabel" runat="server" Text='<%# Eval("UnitPrice") %>' />
                        <br />
                        UnitsInStock:
                    <asp:Label ID="UnitsInStockLabel" runat="server" Text='<%# Eval("UnitsInStock") %>' />
                        <br />
                        UnitsOnOrder:
                    <asp:Label ID="UnitsOnOrderLabel" runat="server" Text='<%# Eval("UnitsOnOrder") %>' />
                        <br />
                        ReorderLevel:
                    <asp:Label ID="ReorderLevelLabel" runat="server" Text='<%# Eval("ReorderLevel") %>' />
                        <br />
                        <asp:CheckBox ID="DiscontinuedCheckBox" runat="server" Checked='<%# Eval("Discontinued") %>' Enabled="false" Text="Discontinued" />
                        <br />
                        <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                        <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                        <br />
                        <br />
                    </span>
                </SelectedItemTemplate>
            </asp:ListView>
            <asp:SqlDataSource ID="sdsNorthwind" runat="server" ConnectionString="<%$ ConnectionStrings:NorthwindConnectionString %>" DeleteCommand="DELETE FROM [tblProducts] WHERE [ProductID] = ?" InsertCommand="INSERT INTO [tblProducts] ([ProductID], [ProductName], [SupplierID], [CategoryID], [QuantityPerUnit], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [ReorderLevel], [Discontinued]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:NorthwindConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [tblProducts]" UpdateCommand="UPDATE [tblProducts] SET [ProductName] = ?, [SupplierID] = ?, [CategoryID] = ?, [QuantityPerUnit] = ?, [UnitPrice] = ?, [UnitsInStock] = ?, [UnitsOnOrder] = ?, [ReorderLevel] = ?, [Discontinued] = ? WHERE [ProductID] = ?" >
                <DeleteParameters>
                    <asp:Parameter Name="ProductID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="ProductID" Type="Int32" />
                    <asp:Parameter Name="ProductName" Type="String" />
                    <asp:Parameter Name="SupplierID" Type="Int32" />
                    <asp:Parameter Name="CategoryID" Type="Int32" />
                    <asp:Parameter Name="QuantityPerUnit" Type="String" />
                    <asp:Parameter Name="UnitPrice" Type="Decimal" />
                    <asp:Parameter Name="UnitsInStock" Type="Int16" />
                    <asp:Parameter Name="UnitsOnOrder" Type="Int16" />
                    <asp:Parameter Name="ReorderLevel" Type="Int16" />
                    <asp:Parameter Name="Discontinued" Type="Boolean" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="ProductName" Type="String" />
                    <asp:Parameter Name="SupplierID" Type="Int32" />
                    <asp:Parameter Name="CategoryID" Type="Int32" />
                    <asp:Parameter Name="QuantityPerUnit" Type="String" />
                    <asp:Parameter Name="UnitPrice" Type="Decimal" />
                    <asp:Parameter Name="UnitsInStock" Type="Int16" />
                    <asp:Parameter Name="UnitsOnOrder" Type="Int16" />
                    <asp:Parameter Name="ReorderLevel" Type="Int16" />
                    <asp:Parameter Name="Discontinued" Type="Boolean" />
                    <asp:Parameter Name="ProductID" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <br/>
            <asp:ValidationSummary ID="vsNorthwind" runat="server" CssClass="error" HeaderText="Please correct the following errors: "/>
            <br/>
            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
        </form>
    </section>
</body>
</html>
