﻿using System;
using System.Web.UI.WebControls;

public partial class ProductMaintenance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            this.lblError.Text = "";
        }
    }

    protected void ListView1_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A data base error has occurred. Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        } 
            else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that product. Please try again.";
        }
            else
        {
            this.lvProducts.DataBind();
        }
    }

    protected void ListView1_ItemDeleted(object sender, ListViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A data base error has occurred. Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that product. Please try again.";
        }
        else
        {
            this.lvProducts.DataBind();
        }
    }
}